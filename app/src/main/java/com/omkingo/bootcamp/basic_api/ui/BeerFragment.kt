package com.omkingo.bootcamp.basic_api.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.omkingo.bootcamp.R
import com.omkingo.bootcamp.basic_api.data.BeerModel
import com.omkingo.bootcamp.basic_api.service.BeerManager
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_beer.btnRefresh
import kotlinx.android.synthetic.main.activity_beer.ivBeer
import kotlinx.android.synthetic.main.activity_beer.tvBeer
import kotlinx.android.synthetic.main.activity_beer.tvBeerDescription

class BeerFragment: Fragment(), BeerInterface {

    companion object {
        fun newInstance(): BeerFragment = BeerFragment()
    }

    private val presenter = BeerPresenter(this, BeerManager().createService())

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.activity_beer, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getBeerApi()
        setView()
    }

    private fun setView() {
        btnRefresh.setOnClickListener{
            presenter.getBeerApi()
        }
    }

    override fun setBeer(beerItem: BeerModel) {
        Picasso.get().load(beerItem.imageUrl).placeholder(R.mipmap.ic_launcher).into(ivBeer)
        tvBeer.text = "${beerItem.name} (${beerItem.abv}%)"
        tvBeerDescription.text = beerItem.description
    }

    override fun showErrorMsg(msg: String) {
        //Show error msg dialog
    }
}
