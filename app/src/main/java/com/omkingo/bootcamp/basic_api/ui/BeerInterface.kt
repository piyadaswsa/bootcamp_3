package com.omkingo.bootcamp.basic_api.ui

import com.omkingo.bootcamp.basic_api.data.BeerModel

interface BeerInterface {

    fun setBeer(beerItem: BeerModel)

    fun showErrorMsg(msg: String)
}
