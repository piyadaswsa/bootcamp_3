package com.omkingo.bootcamp.basic_interface.open_account

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.omkingo.bootcamp.R
import com.omkingo.bootcamp.basic_interface.IntroductionInterface
import kotlinx.android.synthetic.main.introduction.introductionContent

class OpenAccountIntroductionActivity : AppCompatActivity(), IntroductionInterface {

    private val presenter = OpenAccountIntroductionPresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.introduction)
        presenter.init()
    }

    override fun setContent(contentText: String) {
        //Set ui content
        introductionContent.text = contentText
    }

    override fun navigateNext() {
        //ToDo: Navigate to next screen
    }
}