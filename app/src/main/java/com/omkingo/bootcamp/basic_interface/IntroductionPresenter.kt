package com.omkingo.bootcamp.basic_interface

class IntroductionPresenter (private val view: IntroductionInterface) {

    fun init() {
        val information = "Hello User, Welcome to SCB"
        view.setContent(information)
    }

    fun onNextClicked() {
        //ToDo: Call API
        view.navigateNext()
    }
}
