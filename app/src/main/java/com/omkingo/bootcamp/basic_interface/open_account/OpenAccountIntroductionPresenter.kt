package com.omkingo.bootcamp.basic_interface.open_account

import com.omkingo.bootcamp.basic_interface.IntroductionInterface

class OpenAccountIntroductionPresenter(private val view: IntroductionInterface) {

    fun init() {
        //Get content from Api
        val information = "Open Account\n Term and Conditions"
        view.setContent(information)
    }
}