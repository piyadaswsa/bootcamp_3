package com.omkingo.bootcamp.todo

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.omkingo.bootcamp.R
import com.omkingo.bootcamp.basic_interface.IntroductionActivity
import com.omkingo.bootcamp.todo.model.ToDoModel
import kotlinx.android.synthetic.main.activity_todo.btnAdd
import kotlinx.android.synthetic.main.activity_todo.etInputTask
import kotlinx.android.synthetic.main.activity_todo.rvTodo

class ToDoInputActivity : AppCompatActivity() {
    private lateinit var todoAdapter: ToDoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_todo)
        setView()
    }

    private fun setView() {
        val listener = object : ToDoClickListener {
            override fun onItemClick(taskName: String) {
                IntroductionActivity.startActivity(this@ToDoInputActivity, taskName)
            }
        }
        todoAdapter = ToDoAdapter(listener)

        rvTodo.adapter = todoAdapter
        rvTodo.layoutManager = LinearLayoutManager(this)
        rvTodo.itemAnimator = DefaultItemAnimator()

        btnAdd.setOnClickListener {
            val todoModel: ToDoModel = getAddModel(etInputTask.text.toString())
            todoAdapter.addListTask(todoModel)
            etInputTask.text = null
        }
    }

    private fun getAddModel(name: String): ToDoModel {
        return ToDoModel(false, name)
    }

    override fun onDestroy() {
        btnAdd.setOnClickListener(null)
        super.onDestroy()
    }
}