package com.omkingo.bootcamp.fragment_sample

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.omkingo.bootcamp.R
import com.omkingo.bootcamp.basic_api.ui.BeerFragment
import com.omkingo.bootcamp.fragment_sample.data.FragmentModel
import com.omkingo.bootcamp.fragment_sample.ui.main.PlaceholderFragment
import com.omkingo.bootcamp.fragment_sample.ui.main.ProfileFragment
import com.omkingo.bootcamp.fragment_sample.ui.main.SectionsPagerAdapter
import kotlinx.android.synthetic.main.activity_home.tabs
import kotlinx.android.synthetic.main.activity_home.viewPager

class HomeActivity : AppCompatActivity(), HomeInterface {

    private val presenter = HomePresenter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        presenter.setup()
    }

    override fun setView(fmList: List<FragmentModel>) {
        val sectionsPagerAdapter = SectionsPagerAdapter(fmList, supportFragmentManager)
        viewPager.adapter = sectionsPagerAdapter
        tabs.setupWithViewPager(viewPager)
    }
}