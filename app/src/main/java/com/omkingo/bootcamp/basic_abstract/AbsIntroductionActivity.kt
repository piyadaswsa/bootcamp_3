package com.omkingo.bootcamp.basic_abstract

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.omkingo.bootcamp.R
import kotlinx.android.synthetic.main.common_button.btnNext
import kotlinx.android.synthetic.main.introduction.introductionContent

abstract class AbsIntroductionActivity : AppCompatActivity(), AbsIntroductionInterface {

    abstract fun navigateNext()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.introduction)
        setView()
    }

    private fun setView() {
        setContent(getString(R.string.loan_introduction))
        btnNext.setOnClickListener {
            navigateNext()
        }
    }

    override fun setContent(contentText: String) {
        introductionContent.text = contentText
    }
}